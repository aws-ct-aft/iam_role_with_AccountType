locals {

    Build_AccountId = "678984727048"

    Account_Type = data.aws_organizations_resource_tags.Account_Tags.tags["AccountType"]
    Account_Alias = try(data.aws_organizations_resource_tags.Account_Tags.tags["Alias"],"default")
}

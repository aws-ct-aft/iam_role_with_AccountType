data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_iam_policy" "admin-policy" {
  name = "AdministratorAccess"
}

data "aws_ssm_parameter" "aft-execution-role" {
  provider = aws.ct-aft-mgmt
  name = "/aft/resources/iam/aft-execution-role-name"
}

data "aws_organizations_resource_tags" "Account_Tags" {
    provider = aws.ct-mgmt
    resource_id = data.aws_caller_identity.current.account_id
}

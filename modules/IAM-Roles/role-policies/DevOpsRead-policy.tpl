{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "route53:ListHostedZones",
                "route53:GetHostedZone",
                "route53:CreateVPCAssociationAuthorization",
                "ec2:*"
            ],
            "Resource": "*"
        }
    ]
}
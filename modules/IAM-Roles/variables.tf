variable "Build_AccountId" {
    type = string
}

variable "Update_ResourceArn" {
    type = bool
    default = false
}

variable IAM_Role_List {}
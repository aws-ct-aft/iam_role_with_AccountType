
resource "aws_iam_role" "iam_role" {
  for_each = {
    for role in var.IAM_Role_List: role.name => role 
  }
  name = each.key
  assume_role_policy = templatefile("${path.module}/trust-policies/${each.value.policy_prefix}-trust-policy.tpl", { 
    trust_account_id = var.Build_AccountId 
  })
}

resource "aws_iam_role_policy" "iam_role_policy" {
  for_each = {
    for role in var.IAM_Role_List: role.name => role 
  }
  name = "${each.value.policy_prefix}-policy"
  role = aws_iam_role.iam_role["${each.key}"].id
  policy = templatefile("${path.module}/role-policies/${each.value.policy_prefix}-policy.tpl", { none = "none" })
}

resource "aws_ssm_parameter" "SSM_roleArn" {
  for_each = {
    for role in var.IAM_Role_List: role.name => role 
    if var.Update_ResourceArn == true
  }
  provider = aws.build_account 
  name  = "/BuildAccountUpdateResourceArn/${data.aws_caller_identity.current.account_id}/${aws_iam_role.iam_role["${each.key}"].name}"
  type  = "String"
  value = aws_iam_role.iam_role["${each.key}"].arn
  overwrite = true
  depends_on = [aws_iam_role.iam_role]

}


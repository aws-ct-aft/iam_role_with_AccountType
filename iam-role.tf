# Core accounts customization
module "Core-IAM-Role" {
    count = local.Account_Type == "Core" ? 1:0
    source = "./modules/IAM-Roles"

    Build_AccountId = local.Build_AccountId
  
    IAM_Role_List  = [
        {
            name          = "DevOpsRead"
            policy_prefix = "DevOpsRead"
        },
        {
            name          = "DevOpsAdmin"
            policy_prefix = "DevOpsAdmin"
        },
        {
            name          = "CoreAutomation"
            policy_prefix = "CoreAutomation"
        }
    ]

    providers = {
        aws.build_account = aws.build_account
    }
}

# DevOps accounts customization
module "DevOps" {
    count = local.Account_Type == "DevOps" ? 1:0
    source = "./modules/IAM-Roles"

    Build_AccountId = local.Build_AccountId
    
    IAM_Role_List  = [
        {
            name          = "DevOpsRead"
            policy_prefix = "DevOpsRead"
        },
        {
            name          = "DevOpsAdmin"
            policy_prefix = "DevOpsAdmin"
        }
    ]

    Update_ResourceArn    = true

    providers = {
        aws.build_account = aws.build_account
    }
}

# App accounts customization
module "App" {
    count = local.Account_Type == "App" ? 1:0
    source = "./modules/IAM-Roles"

    Build_AccountId = local.Build_AccountId
    
    IAM_Role_List  = [
        {
            name          = "DevOpsAdmin"
            policy_prefix = "DevOpsAdmin"
        }
    ]

    Update_ResourceArn    = true

    providers = {
        aws.build_account = aws.build_account
    }
}